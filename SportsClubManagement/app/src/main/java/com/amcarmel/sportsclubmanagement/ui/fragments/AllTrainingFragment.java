package com.amcarmel.sportsclubmanagement.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.amcarmel.sportsclubmanagement.R;
import com.amcarmel.sportsclubmanagement.logic.INetworkListener;
import com.amcarmel.sportsclubmanagement.models.Coach;
import com.amcarmel.sportsclubmanagement.models.Training;
import com.amcarmel.sportsclubmanagement.models.TrainingInstance;
import com.amcarmel.sportsclubmanagement.models.TrainingInstanceList;
import com.amcarmel.sportsclubmanagement.models.User;
import com.amcarmel.sportsclubmanagement.ui.activities.MainActivity;
import com.amcarmel.sportsclubmanagement.ui.adapters.TrainingsRecyclerViewAdapter;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllTrainingFragment extends FragmentBase implements View.OnClickListener, INetworkListener{

    private static final String TAG = AllTrainingFragment.class.getSimpleName();
    private RecyclerView _trainingRecyclerView;
    private ProgressBar _progressBar;
    private ArrayList<TrainingInstance> _trainingsList;
    private TrainingsRecyclerViewAdapter _adapter;
    private Button _getTrainingsBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_all_training, container, false);
        initViews(v);
        return v;
    }

    private void initViews(View v) {       

        _trainingRecyclerView = v.findViewById(R.id.trainings_recycler_view);
        _trainingRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        _progressBar = v.findViewById(R.id.progress_bar);
        enableProgressBar(false);
        _trainingsList = new ArrayList<>();
        _adapter = new TrainingsRecyclerViewAdapter(getActivity(), _trainingsList);
        _trainingRecyclerView.setAdapter(_adapter);

        _getTrainingsBtn = v.findViewById(R.id.get_training_list_button);

        _getTrainingsBtn.setOnClickListener(this);
    }

    private void enableProgressBar(final boolean enable){
        if(_progressBar == null){
            return;
        }
        _progressBar.post(new Runnable() {
            @Override
            public void run() {
                _progressBar.setVisibility(enable ? View.VISIBLE : View.INVISIBLE);
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.get_training_list_button:
                handleGetTrainingBtnClick();
                break;
            default:
                break;
        }
    }

    private void handleGetTrainingBtnClick() {

        Log.w(TAG, "handleGetTrainingBtnClick");
        enableProgressBar(true);
        ((MainActivity)getActivity()).getAllTrainings(this);

        //addMockDataToList();
    }

    private void addMockDataToList() {
        TrainingInstance instance = new TrainingInstance();
        instance.Coach = new Coach();
        instance.Coach.User.FirstName = "Loca";
        instance.Coach.User.LastName = "Modrich";
        instance.Timestamp = BigInteger.valueOf(System.currentTimeMillis());
        instance.Id = 1;

        _trainingsList.add(instance);
        _adapter.notifyDataSetChanged();
        enableProgressBar(false);
    }

    @Override
    public void onReceivedUserTrainings(TrainingInstanceList list) {

        if (list == null){
            Log.w(TAG, "All Training List Fragment received a null training list.");
            return;
        }

        _trainingsList.clear();
        _trainingsList.addAll(list);

        Activity a = getActivity();

        if (a != null){

            a.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    enableProgressBar(false);
                    _adapter.notifyDataSetChanged();
                }
            });
        }
    }
}
