package com.amcarmel.sportsclubmanagement.models;

import java.math.BigInteger;
import java.util.List;

public class TrainingInstance {
    public Integer Id;

    public Training Training;

    public BigInteger Timestamp;

    public List<Trainee> Trainees;

    public Coach Coach;
}
