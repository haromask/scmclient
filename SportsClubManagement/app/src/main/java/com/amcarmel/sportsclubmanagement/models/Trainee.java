package com.amcarmel.sportsclubmanagement.models;

import java.util.List;

public class Trainee{

    public User User;

    public int MaxLevel;

    public String Gender;

    public List<Training> Trainings;

    public List<TrainingInstance> TrainingInstances;
}
