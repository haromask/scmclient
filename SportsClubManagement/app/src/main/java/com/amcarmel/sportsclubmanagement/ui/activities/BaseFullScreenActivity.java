package com.amcarmel.sportsclubmanagement.ui.activities;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.amcarmel.sportsclubmanagement.R;
import com.amcarmel.sportsclubmanagement.ui.fragments.FragmentBase;

public abstract class BaseFullScreenActivity extends AppCompatActivity {

    private static final String TAG = BaseFullScreenActivity.class.getSimpleName();
    private int currentApiVersion;
    protected ProgressBar _progressBar;

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_full_screen);

        _progressBar = (ProgressBar) findViewById(R.id.activity_progress_bar);

        currentApiVersion = android.os.Build.VERSION.SDK_INT;

        // This work only for android 4.4+
        if (currentApiVersion >= Build.VERSION_CODES.KITKAT) {

            final int flags = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

            getWindow().getDecorView().setSystemUiVisibility(flags);

            // Code below is to handle presses of Volume up or Volume down.
            // Without this, after pressing volume buttons, the navigation bar will
            // show up and won't hide
            final View decorView = getWindow().getDecorView();
            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {

                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if ((visibility & View.SYSTEM_UI_FLAG_HIDE_NAVIGATION) == 0) {
                        decorView.setSystemUiVisibility(flags);
                    }
                }
            });
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        if (currentApiVersion >= Build.VERSION_CODES.KITKAT && hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
            finish();
        }
    }

    protected void replaceFragment(final Class<? extends FragmentBase> fragmentClass, final Bundle args) {

        final FragmentManager fragmentManager = getFragmentManager();
        Fragment newFrag;

        try {
            newFrag = fragmentClass.newInstance();
            newFrag.setArguments(args);

        } catch (final Exception e) {
            e.printStackTrace();
            return;
        }

        final FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.replace(R.id.fragments_container, newFrag, fragmentClass.getSimpleName());
        transaction.addToBackStack(fragmentClass.getSimpleName()).commitAllowingStateLoss();
    }

    protected void showProgressBar(final boolean show) {
        if (_progressBar == null) {
            return;
        }

        _progressBar.post(new Runnable() {
            @Override
            public void run() {
                _progressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
            }
        });
    }
}
