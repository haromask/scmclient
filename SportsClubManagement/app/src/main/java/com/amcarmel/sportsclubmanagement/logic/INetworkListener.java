package com.amcarmel.sportsclubmanagement.logic;

import com.amcarmel.sportsclubmanagement.models.TrainingInstanceList;

/**
 * Created by amcarmel on 7/24/2018.
 */

public interface INetworkListener {
    void onReceivedUserTrainings(TrainingInstanceList list);
}
