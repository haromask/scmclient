package com.amcarmel.sportsclubmanagement.ui.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amcarmel.sportsclubmanagement.R;
import com.amcarmel.sportsclubmanagement.logic.IFragmentBackPressListener;
import com.amcarmel.sportsclubmanagement.ui.activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentBase extends Fragment implements IFragmentBackPressListener {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_base, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).registerToBackPress(this);
    }

    @Override
    public void onPause() {
        ((MainActivity)getActivity()).registerToBackPress(null);
        super.onPause();
    }


    @Override
    public boolean onBackPressed() {
        return false;
    }

}
