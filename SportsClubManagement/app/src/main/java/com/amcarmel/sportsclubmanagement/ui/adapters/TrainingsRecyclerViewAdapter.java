package com.amcarmel.sportsclubmanagement.ui.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amcarmel.sportsclubmanagement.R;
import com.amcarmel.sportsclubmanagement.models.TrainingInstance;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.List;

/**
 * Created by amcarmel on 7/24/2018.
 */

public class TrainingsRecyclerViewAdapter extends RecyclerView.Adapter<TrainingsRecyclerViewAdapter.CustomViewHolder>{

    private List<TrainingInstance> trainingList;
    private Context mContext;

    public TrainingsRecyclerViewAdapter(Context context, List<TrainingInstance> trainingList) {
        this.trainingList = trainingList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.training_instance_item_layout, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int i) {
        TrainingInstance instance = trainingList.get(i);

        //Setting text view title
        holder.mainTextView.setText("Level: " + instance.Training.Level);
        holder.secondTextView.setText("Date : " + instance.Timestamp);
        holder.coachNameTextView.setText("Coach: " + instance.Coach.User.FirstName + " " + instance.Coach.User.LastName);

        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse("https://tmssl.akamaized.net//images/portrait/originals/27992-1413206775.jpg"))
                .build();

        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .setOldController(holder.coachProfileImage.getController())
                .build();

        holder.coachProfileImage.setController(controller);

    }

    @Override
    public int getItemCount() {
        return (null != trainingList ? trainingList.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView coachNameTextView;
        protected SimpleDraweeView coachProfileImage;
        protected TextView mainTextView;
        protected TextView secondTextView;

        public CustomViewHolder(View view) {
            super(view);
            this.mainTextView = view.findViewById(R.id.training_instance_name);
            this.secondTextView = view.findViewById(R.id.training_instance_time);
            this.coachNameTextView = view.findViewById(R.id.training_coach_name_text);
            this.coachProfileImage = view.findViewById(R.id.training_coach_profile_image);
        }
    }
}
