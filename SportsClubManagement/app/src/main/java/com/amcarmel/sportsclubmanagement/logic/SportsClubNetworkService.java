package com.amcarmel.sportsclubmanagement.logic;

import com.amcarmel.sportsclubmanagement.models.TrainingInstanceList;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by amcarmel on 7/24/2018.
 */

public interface SportsClubNetworkService {

    @GET("traininginstances")
    Call<TrainingInstanceList> getUserTrainings();
}
