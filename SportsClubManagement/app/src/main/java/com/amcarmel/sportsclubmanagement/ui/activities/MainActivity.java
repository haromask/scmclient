package com.amcarmel.sportsclubmanagement.ui.activities;

import android.os.Bundle;

import com.amcarmel.sportsclubmanagement.logic.IFragmentBackPressListener;
import com.amcarmel.sportsclubmanagement.logic.INetworkListener;
import com.amcarmel.sportsclubmanagement.logic.NetworkLogic;
import com.amcarmel.sportsclubmanagement.ui.fragments.AllTrainingFragment;
import com.facebook.drawee.backends.pipeline.Fresco;

public class MainActivity extends BaseFullScreenActivity {

    private IFragmentBackPressListener _fragbackPressListener;
    private NetworkLogic _networkLogic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {

        Fresco.initialize(this);
        _networkLogic = new NetworkLogic();
        replaceFragment(AllTrainingFragment.class, null);
    }

    public void registerToBackPress(IFragmentBackPressListener listener) {
        _fragbackPressListener = listener;
    }

    public void getAllTrainings(INetworkListener listener) {

        if (_networkLogic != null){
            _networkLogic.getUserTrainings(listener);
        }
    }
}
