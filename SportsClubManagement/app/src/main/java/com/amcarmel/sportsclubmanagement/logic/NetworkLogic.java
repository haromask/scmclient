package com.amcarmel.sportsclubmanagement.logic;

import android.util.Log;

import com.amcarmel.sportsclubmanagement.models.TrainingInstanceList;

import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkLogic {

    private static final String BASE_URL = "http://scmserver.azurewebsites.net/api/";
    private final SportsClubNetworkService _service;

    public NetworkLogic(){

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        //httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        _service = retrofit.create(SportsClubNetworkService.class);
    }

    public void getUserTrainings(final INetworkListener listener){

        Call<TrainingInstanceList> call = _service.getUserTrainings();

        call.enqueue(new Callback<TrainingInstanceList>() {
            @Override
            public void onResponse(Call<TrainingInstanceList> call, Response<TrainingInstanceList> response) {
                Log.w(getClass().getSimpleName(), "onResponse : call = " + call + " response = " + response);
                TrainingInstanceList list = response.body();

                if (listener != null) {
                    listener.onReceivedUserTrainings(list);
                }
            }

            @Override
            public void onFailure(Call<TrainingInstanceList> call, Throwable t) {
                Log.w(getClass().getSimpleName(), "onFailure : call = " + call + " t = " + t);
            }
        });
    }
}
