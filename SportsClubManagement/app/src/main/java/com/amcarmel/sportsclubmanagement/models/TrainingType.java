package com.amcarmel.sportsclubmanagement.models;

enum TrainingType {
    Fitness,
    Volleyball
}
