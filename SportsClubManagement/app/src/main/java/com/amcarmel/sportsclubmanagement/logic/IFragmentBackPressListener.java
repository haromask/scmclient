package com.amcarmel.sportsclubmanagement.logic;

/**
 * Created by amcarmel on 7/24/2018.
 */

public interface IFragmentBackPressListener {
    boolean onBackPressed();
}
