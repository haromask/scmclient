package com.amcarmel.sportsclubmanagement.models;

import java.util.List;

public class Training {
    public List<Trainee> Trainees;

    public int Level;

    public String Gender;

    public TrainingType Type;

    public int MaxTrainees;

    public Coach Coach;

    public String Location;
}
